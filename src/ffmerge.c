#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>

int c, errorcode, linenr, recordingday, mmsb, msb, lsb, hours,
  minutes, seconds, rectime, d, bytecount1, bytecount2;
int padding = 165, error = 0, option_index = 0,
  reclen = 0, fileq = 0, inputfileid = -1,
  validexitcode = 0, mode = 0, a5 = 165, minrectime = 8639999,
  ptraccfilename = 0, mininputfileid = -1, validmemcode = 0;
FILE *f, *g;
char *outputfile = NULL, *accfilename = NULL,
  *filename = NULL;
unsigned char *y;
int *ptrbytecount1, *ptrbytecount2, *ptrerrorcode, *ptrlinenr,
  *ptrrecordingday, *ptrmmsb, *ptrmsb, *ptrlsb, *ptrreclen, *offset;

void printusage(int mode) {
  if (mode == 0) {
    /* We print to standard output*/
    printf ("Usage: ffmerge [ -i input files -o output file ]\n"
    "Merges Final Format files\n"
    "             [ -h this help listing ]\n"
    "             [ -v prints the version ]\n");
  } else {
    /* We print to standard error*/
    fprintf (stderr,"Usage: ffmerge [ -i input files -o output file ]\n"
    "Merges Final Format files\n"
    "             [ -h this help listing ]\n"
    "             [ -v prints the version ]\n");
  }
}

int getreclen() {
  /* This is the first time that we try to read a file*/
  bytecount1 = fgetc(f);
  if (bytecount1 != -1 ) {
    bytecount2 = fgetc(f);
    reclen= bytecount2 + bytecount1 * 256;
  } 
  else {
    /* If bytecount1 = -1, then
       bytecount2 and reclen also get the -1 value*/
    bytecount2 = -1;
    reclen = -1;
  }
  *(ptrbytecount1 + inputfileid) = bytecount1;
  *(ptrbytecount2 + inputfileid) = bytecount2;
  *(ptrreclen + inputfileid) = reclen;
  return reclen;
}

int geterrorcode() {
  c = fgetc(f);
  *(ptrerrorcode + inputfileid) = c;
  return c;
}

int getlinenr() {
  c = fgetc(f);
  *(ptrlinenr + inputfileid) = c;
  return c;
}

int getrecordingday() {
  c = fgetc(f);
  *(ptrrecordingday + inputfileid ) = c;
  return c;
}

int getmmsb() {
  c = fgetc(f);
  *(ptrmmsb + inputfileid ) = c;
  return c;
}

int getmsb() {
  c = fgetc (f);
  *(ptrmsb + inputfileid ) = c;
  return c;
}

int getlsb() {
  c = fgetc (f);
  *(ptrlsb + inputfileid) = c;
  return c;
}

int printdump(unsigned char *y) {
  fprintf (g, "%c", *(ptrbytecount1 + inputfileid));
  fprintf (g, "%c", *(ptrbytecount2 + inputfileid));
  fprintf (g, "%c", *(ptrerrorcode + inputfileid));
  fprintf (g, "%c", *(ptrlinenr + inputfileid));
  fprintf (g, "%c", *(ptrrecordingday + inputfileid));
  fprintf (g, "%c", *(ptrmmsb + inputfileid));
  fprintf (g, "%c", *(ptrmsb + inputfileid));
  fprintf (g, "%c", *(ptrlsb + inputfileid));
  return 0;
}

int getpadding() {
  int i;
  for (i = 0; i < 4 ; i++) {
    c = fgetc(f);
    if (c != 165) {
      padding = 0;
    } else {
      fprintf (g, "%c", c);
    }
  }
  return padding;
}

void inputfileq() {
  int flag = 0;
  int i;
  /* fileq denotes the quantity of input files that we are to process
     here we determine fileq 
     fileq and flag are declared having a vulue of 0 */
  for (i = 0; optarg[i] != '\0'; i++) {
    if (optarg[i] != ' ' && optarg[i] != ',') {
      /* We have a filename */
      if (flag == 0) {
        /* This is the rising edge of the filename*/
        fileq++;
        flag = 1;
      }
      /* flag will be lowered to value of 0 when a white space or a comma is encountered*/
    } else {
      /* falling edge of the filename*/
      flag = 0;
    }
  }
}

int allocatemem() {
  ptrbytecount1 = malloc (fileq * sizeof (int));
  ptrbytecount2 = malloc (fileq * sizeof (int));
  ptrlinenr = malloc (fileq * sizeof (int));
  ptrreclen = malloc (fileq * sizeof (int));
  ptrerrorcode = malloc (fileq * sizeof (int));
  ptrrecordingday = malloc (fileq * sizeof (int));
  ptrmmsb = malloc (fileq * sizeof (int));
  ptrmsb = malloc (fileq * sizeof (int));
  ptrlsb = malloc (fileq * sizeof (int));
  ptrreclen = malloc (fileq * sizeof (int));
  offset = malloc (fileq * sizeof (int));
  if (ptrbytecount1 == 0 || ptrbytecount2 == 0 || ptrlinenr == 0 || 
    ptrreclen == 0 || ptrerrorcode == 0 || ptrrecordingday == 0 ||
    ptrmmsb == 0 || ptrmsb == 0 || ptrlsb == 0 || offset == 0) {
    /* memory allocation failure condition */
    return 1;
  }
  else  {
    return 0;
  }
}

int getinputfile() {
  int flag = 0;
  int i;
  int j = 0;
  char *argfilename = NULL;
  for (i = 0; optarg[i] != '\0'; i++) {
    if (optarg[i] != ' ' && optarg[i] != ',') {
      flag = 1;
      argfilename = realloc(argfilename, sizeof(int) * (j + 1));
      argfilename[j] = optarg[i];
      j++;
      /* string accfilename contains a cleaned version of optarg 
         while ptraccfilename is its index */
      accfilename = realloc(accfilename, sizeof(int) * (ptraccfilename + 1));
      accfilename[ptraccfilename] = optarg[i];
      ptraccfilename++;
    } else if (flag == 1)  {
      argfilename[j] = 0;
      accfilename[ptraccfilename] = 0;
      ptraccfilename++;
      j = 0;
      flag = 0;
    }
  }
  if (flag == 1) {
    argfilename[j] = 0;
    accfilename[ptraccfilename] = 0;
    ptraccfilename++;
    /*
    f = fopen(argfilename, "r");
    if (f == 0) {
      fprintf (stderr, "Unable to open input file %s\n", filename);
    }
    */
  }
  free(argfilename);
  return 0;
}

void getfilename (){
  int zeroq = 0;
  int i;
  int j = 0;
  for (i = 0; i < ptraccfilename + 1; i++) {
    if (accfilename[i] == 0) {
      zeroq++;
    }
    else if (zeroq == inputfileid) {
      filename = realloc(filename, sizeof(char) * (j + 1));
      filename[j]=accfilename[i];
      j++;
    }
  }
  filename[j] = 0;
}

void freememory() {
  fclose(g);
  free(y);
  free(accfilename);
  free(filename);
  free(ptrbytecount1);
  free(ptrbytecount2);
  free(ptrerrorcode);
  free(ptrlinenr);
  free(ptrrecordingday);
  free(ptrmmsb);
  free(ptrmsb);
  free(ptrlsb);
  free(ptrreclen);
  free(offset);
}

int main (int argc, char *argv[])
{
  /* int iteration; */
  int i;
  int validinputfile = 1;

  while (1)
  {
  static struct option long_options[] =
    {
    /* These options don’t set a flag.
       We distinguish them by their indices. */
      {"version",     no_argument,       0, 'v'},
      {"help",  no_argument,       0, 'h'},
      {"inputfile",  required_argument, 0, 'i'},
      {"outputfile",  required_argument, 0, 'o'},
      {0, 0, 0, 0}
    };
    /* getopt_long stores the option index here. */
    int option_index = 0;
    d = getopt_long (argc, argv, "vhi:o:",
                       long_options, &option_index);
    /* Detect the end of the options. */
    if (d == -1) {
      break;
    }
    switch (d)
    {
      case 'h':
        printusage(0);
        return 0;
        break;
      case 'v':
        printf ("ffmerge version 1.0\n");
        return 0;
        break;
      case 'o':
        outputfile = optarg;
        g = fopen(outputfile, "w");
        if (g == 0) {
          fprintf (stderr, "Unable to open output file %s\n", outputfile);
          return 1;
        }
        break;
      case 'i':
        inputfileq();
        validinputfile = getinputfile();
        break;
      default:
        fprintf(stderr, "Option is incorrect\n");
        return 1;
    }
  }

  if (validinputfile != 0) {
    fprintf (stderr, "No input file selected\n\n");
    printusage (1);
    return 1;
   }  

  if (g == 0) {
    fprintf (stderr, "No output file selected \n\n");
    printusage(1);
    return 1;
  }

  validmemcode = allocatemem();
  if (validmemcode == 1) {
    fprintf (stderr, "memory allocation failure \n");
    return 1;
  }

  /* We set an initial reclen of 0 for all inputfiles */
  for (i = 0; i < fileq; i++) {
    *(ptrreclen + i) = 0; 
    *(offset + i) = 0;
  }

  while (1) {
    /* This is an infinite loop, but it
       will be terminated when all files give
       reclen = -1

       Now, inputfileid takes the value of mininputifileid at the end of this
       loop, just before we call getpadding*/
    for (i = 0; i < fileq; i++) {
       if (i == inputfileid) {
         *(offset + i) += *(ptrreclen + i);
       }
    }
    /* Set minrectime the number of seconds in a day * 100 - 1 
       This is the maximum value minrectime can take */
    minrectime = 8639999;
    for (inputfileid = 0 ;inputfileid < fileq; inputfileid++) {
      /* We go through the input files */
      getfilename();
      if (filename == NULL) {
        fprintf (stderr, "We need at least one Input File\n");
        printusage (1);
        return 1;
      }
      f = fopen(filename, "r");
      /* We open the file for the first time */
      if ( f == 0 ) {
        /* We did not find the input file */
        fprintf (stderr,"%s not found\n\n", filename);
        printusage(1);
        return 1;
      }
      /* *offset will set by the reclen of the previous reclen (*ptrreclen) */
      fseek (f, *(offset + inputfileid), SEEK_SET);
      reclen=getreclen();
      if (bytecount1 != -1 && bytecount2 == -1 ) {
        fprintf (stderr, "%s: wrong format\n\n", filename);
        printusage(1);
        return 1;
      }
      /* reclen = -1 is the only condition that we accept for an encounter with
         an EOF, so if reclen = -1, do not decode the file any further, to save
         computation time */
      if (reclen != -1) {
        errorcode = geterrorcode();
        linenr = getlinenr();
        recordingday = getrecordingday();
        mmsb = getmmsb();
        msb = getmsb();
        lsb = getlsb();
      }
      /* So errorcode, linenr, recordingday, mmsb, msb or lsb should never have
      a value of -1, unless we are dealing with a rogue file (or user :-) */
      if (errorcode == -1 || 
        linenr == -1 || 
        recordingday == -1 ||
        mmsb == -1 ||
        msb == -1 ||
        lsb == -1) {
        fprintf (stderr, "%s: wrong format\n\n", filename);
        return 1;
      }
      if (reclen != -1) {
        /* reclen = -1 if we are the EOF */
        rectime = mmsb * 65536 + msb * 256 + lsb;
      } else {
        /* If we are at the EOF, set reclen to the maximum value */
        rectime = 8639999;
      }
      if (rectime < minrectime) {
        /* minrectime is the smallest recording time reported by the 
           inputfilest*/
        minrectime = rectime;
        /* mininputfileid is the inputfileid for the file that reported the 
           smallest recording time*/
        mininputfileid = inputfileid;
      }
      /* We Close the inputfile for the first time*/
      fclose(f);
      /* As we have to open the inputfiles a large number of time, we have to
         close them each time due to OS limitations*/
    }
    /* If all inputfiles are empty, then rectime and minrectime are both the
       maximum value of 8639999, so 
       mininputfileid = -1, the original default value*/
    if (mininputfileid == -1) {
      fprintf (stderr, "No records found\n\n");
      return 1;
    }
    inputfileid = mininputfileid;
    if (*(ptrreclen + inputfileid) != -1 && *(ptrreclen + inputfileid) != 0) {
      validexitcode = printdump(y);
    } else {
      /* A file that have reached the end 
         get a reclen = -1 from printdump
         and rectime = 8639999 (the maximum possible value)
         so if the minputfile
         (file with the smallest rectime )
         have a reclen  = -1
         all files must have reached the end
         and so we stop here*/
      return 0;
    }
    for (inputfileid = 0 ;inputfileid < fileq; inputfileid++) {
    /* Clearly imputfileid loses its relation to mininputfileid from 
       here onwards */
      if ( inputfileid == mininputfileid) {
        /* Here inputfileid takes the value of mininputfileid */
        getfilename();
        /* We open the inputfiles a second time again
           but this time only the file with the smallest rectime is opened */
        f = fopen(filename, "r");
      }
    }
    /* We offset another 8 because the Final Format header comprises of 8 
      bytes*/
    fseek (f, *(offset + mininputfileid) + 8, SEEK_SET);
    /* reclen (and therefore, also *ptrreclen) is the length of the whole record.
       We have 8 header bytes and 4 padding for a total of 12, which we must deduct
       to be left with the (ASTERIX) payload */
    for (i = 0; i < *(ptrreclen + mininputfileid) - 12; i++) {
      c = fgetc(f);
      if (c != -1)  {
        fprintf (g, "%c", c);
      }
      else {
        fprintf (stderr, "%s: wrong format\n\n", filename);
        return 1;
      }
    }
    /* So now we set inputfileid back to mininputfileid */
    inputfileid = mininputfileid;

    if (*(ptrreclen + inputfileid) != -1) {
      padding = getpadding();
      if (padding == 0) {
        fprintf (stderr, "%s: wrong format\n\n", filename);
        return 1;
      }
    } 
    fclose(f);
  }
  freememory();
  return 0;
}
